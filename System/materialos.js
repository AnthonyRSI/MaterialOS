window.MaterialOS = null;
(function (document, tag) {
    var scriptTag = document.createElement(tag),
        firstScriptTag = document.getElementsByTagName(tag)[0],
        Checklib = function(){
            if(typeof $ != 'undefined') {
                var s = document.createElement('script'),
                    f = document.getElementsByTagName('script')[1];
                s.src = './System/Core/materialos.js';
                f.parentNode.insertBefore(s, f);
                document.getElementsByTagName('script')[2].remove();
            } else {
                setTimeout(function(){
                    Checklib();
                }, 500);
            }
        };
    document.addEventListener('MaterialOS_Boot', function(){
        return MaterialOS;
    });
    scriptTag.src = './System/Library/jQuery/jquery.min.js';
    firstScriptTag.parentNode.insertBefore(scriptTag, firstScriptTag);
    Checklib();
}(document, 'script'));