/**
 * Created by Anthony on 26/07/2017.
 */
$(function () {
    var MaterialOS_Manager = (function () {
        this.Paths = {
            System: './System/',
            Styles: './System/Styles/',
            Library: './System/Library/',
            Programs: './Programs/',
            Views: './System/Core/views/'
        };
        this.Template = {};
        this.Controllers = {};
        this._id_Container = 'materialos';
        this.Container = $('#' + this._id_Container);
        this.Body = $('body');

        this.init = function () {
            this.loadConfig();
            return this;
        };

        this.addLog = function (text, marge) {
            this.ContainerLog.prepend('<p class="log">' + text + (marge ? '<label class="system-text">' : '') + '</p>');
            return this;
        }

        this.addSubLog = function (text) {

            this.ContainerLog.find('p:first').after('<p class="log log-small">' + text + '</p>');
            return this;
        }

        this.setLastAction = function (bool) {
            this.ContainerLog.find('p:first').append(
                (bool ? '<label class="system-text color-green">OK</label>' : '<label class="system-text color-red">ERREUR</label>')
            );
            return this;
        }

        this.setLastActionToSub = function (bool) {
            this.ContainerLog.find('p:first').next().append(
                (bool ? '<label class="system-text color-green">OK</label>' : '<label class="system-text color-red">ERREUR</label>')
            );
            return this;
        }

        this.loadConfig = function () {
            var self = this;
            this.Body.addClass('boot');
            this.Container.html('<div class="log-group"></div>');
            this.ContainerLog = $('.log-group');
            this.addLog('Chargement du fichier de configuration');
            $.getJSON(this.Paths.System + 'config.json').done(function (data) {
                self.Config = data;
                self.setLastAction(true).loadStyle();
                // self.addLog('Préparation du chargement des librairies', true);
            });
            return this;
        };

        this.loadStyle = function () {
            var self = this;
            this.addLog('Chargement du thème <strong>' + Config.Style.toUpperCase() + '</strong>');
            this.Paths.currentStyle = this.Paths.System + 'Styles/' + this.Config.Style + '/';
            $("head").append('<link rel="stylesheet" href="' + this.Paths.System + 'Styles/' + this.Config.Style + '/system.css"/>');
            $.get(this.Paths.currentStyle + 'Boot.html').done(function (bootTemplate) {
                self.setLastAction(true);
                self.addLog('Préparation du chargement des librairies', true);
                self.loadBootElements(0);
                self.Template.BOOT_MODEL = bootTemplate;
            }).fail(function () {
                self.setLastAction(false);
            })
            return this;
        };

        this.loadBootElements = function (index) {
            var numberElement = this.Config.onBoot.loadLibrary.length,
                self = this;
            if (index == numberElement) {
                self.startBoot();
            } else {
                var library = this.Config.onBoot.loadLibrary[index];
                this.addLog('Chargement de la librairie <strong>' + library.toUpperCase() + '</strong>')
                $.getJSON(this.Paths.Library + library + '/map.json').done(function (mapData) {
                    self.loadLibraryMapFiles(library, mapData, function (result) {
                        if (result.response) {
                            self.setLastAction(true);
                            index++;
                            self.loadBootElements(index);
                        } else {
                            self.setLastAction(false).addLog(result.erreur).setLastAction(false);
                        }
                    })
                }).fail(function () {
                    self.setLastAction(false);
                })
            }
            return this;
        };

        this.loadLibraryMapFiles = function (name, map, onReady) {
            this._loadLibFilesMap(0, 0, name, map, function (result) {
                onReady(result);
            });
            return this;
        };

        this._loadLibFilesMap = function (index, indexStep, name, map, onReady) {
            var step = ['css', 'js'],
                stepName = step[indexStep],
                self = this;
            if (typeof map[stepName] != 'undefined') {
                if (index == map[stepName].length) {
                    indexStep++;
                    self._loadLibFilesMap(0, indexStep, name, map, onReady);
                } else {
                    if (stepName == 'js') {
                        self.addSubLog(map[stepName][index]);
                        $.getScript(this.Paths.Library + name + '/' + map[stepName][index]).done(function () {
                            index++;
                            self.setLastActionToSub(true);
                            self._loadLibFilesMap(index, indexStep, name, map, onReady);
                        }).fail(function () {
                            onReady({response: false, erreur: 'Impossible de récupérer le fichier ' + map[stepName][index]});
                        });
                    } else if (stepName = 'css') {
                        $("head").append('<link rel="stylesheet" href="' + this.Paths.Library + name + '/' + map[stepName][index] + '"/>');
                        self.addSubLog(map[stepName][index]).setLastActionToSub(true);
                        index++;
                        self._loadLibFilesMap(index, indexStep, name, map, onReady);
                    }
                }

            } else {
                if (typeof onReady == 'function')
                    onReady({response: true});
            }
            return this;
        };

        this.startBoot = function () {
            this.Container.append(this.getModel('BOOT_MODEL', {
                ROOT: this.Paths.currentStyle,
                APPNAME: this.Config.AppName
            }));
            this.bootLog = $('.text-boot-event');
            this.ProgramsList = {};
            this.loadPrograms(0);

            var calcTop = '2%'
            $('.bootContent').css({
                marginTop: calcTop
            });
            return this;
        }

        this.loadPrograms = function (index) {
            var self = this;
            if (index == this.Config.Programs.length) {
                this.bootLog.html('Chargement de votre écran d\'accueil...');
                this.loadDesktop();
            } else {
                this.bootLog.html('Chargement de l\'application <strong>' + this.Config.Programs[index] + '</strong>....');
                $.getJSON(this.Paths.Programs + this.Config.Programs[index] + '/App.json').done(function (data) {
                    self.ProgramsList[self.Config.Programs[index]] = data;
                    index++;
                    self.loadPrograms(index);
                }).fail(function () {
                    self.SystemAlert('Le programme <strong>' + self.Config.Programs[index] + '</strong> est introuvable.', 'warning', 10000);
                    index++;
                    self.loadPrograms(index);
                });
            }
        }

        this.getModel = function (modelName, obj) {
            if (this.Template[modelName] == undefined) return 'Model not found';
            var src = this.Template[modelName];
            if (typeof obj == 'object') {
                for (var i in obj) {
                    src = src.replace('{' + i + '}', obj[i]);
                }
            }
            return src;
        }

        this.SystemAlert = function (message, style, timeDestroy) {
            function stringGen(len) {
                var text = "", charset = "abcdefghijklmnopqrstuvwxyz0123456789";
                for (var i = 0; i < len; i++)
                    text += charset.charAt(Math.floor(Math.random() * charset.length));
                return text;
            }

            var calcBottom = 10;
            this.Container.find('.systemAlert').each(function () {
                calcBottom += $(this).height() + 30;
            });
            var winId = stringGen(10);
            this.Container.append('<div data-winId="' + winId + '" class="zoomIn animated systemAlert ' + style + '" style="bottom:' + calcBottom + 'px"><p>' + message + '</p></div>');

            this.Container.find('.systemAlert').unbind().click(function () {
                $(this).remove();
            });

            if (timeDestroy != undefined) {
                setTimeout(function () {
                    this.Container.find('.systemAlert[data-winId="' + winId + '"]').removeClass('zoomIn animated').addClass('zoomOut animated').remove();
                }, timeDestroy);
            }
        }

        this.SystemCenterEvent = function (message) {
            this.close = function () {
                $('.systemCenterEvent').fadeOut(500, function () {
                    $(this).remove();
                });
                return this;
            }
            this.Container.append('<div class="zoomIn animated systemCenterEvent fast-animation"><p>' + message + '</p></div>');
            return this;
        }

        this.loadDesktop = function () {
            if (this.DesktopLoaded) return;
            this.DesktopLoaded = true;
            var self = this;
            $.get(this.Paths.Views + '/UI.html').done(function (data) {
                self.Container.append(data);
                setTimeout(function () {
                    self.Container.find('.log-group').remove();
                    self.Body.removeClass('boot').addClass('running')
                    self.Container.find('#boot').fadeOut(1000, function () {
                        self.Container.find('#boot, style').remove();
                        self.Container.find('#UI').fadeIn(600);
                    });
                    self.Container.css('height', $(window).height() + 'px');
                    self.showTimeSystem();
                }, self.Config.onBoot.preloadTime);
            }).fail(function () {
                self.SystemAlert('Impossible de charger l\'interface de l\'écran d\'accueil.', 'critical', 600000);
            });
        }

        this.showTimeSystem = function () {
            var date = new Date(),
                self = this,
                hour = (date.getHours() < 10 ? '0' : '') + date.getHours() + ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
            $('h3[id="time-system"]').html(hour);
            setTimeout(function () {
                self.showTimeSystem();
            }, 1000);
        }

        this.init();

        return this;
    });
    window.MaterialOS = MaterialOS_Manager();
})