$(function () {
    var MaterialOS = window.MaterialOS;
    MaterialOS.Controllers.UI = (function () {
        var _UI = this;
        this.Mouse = {};
        this.uiStartMenu = $('.start-menu-form');
        this.animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

        this.isSmartphone = function () {
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;
            if (/windows phone/i.test(userAgent)) {
                return true;
            }
            if (/android/i.test(userAgent)) {
                return true;
            }
            if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                return true;
            }
            return false
        }

        this.Taskbar = function (process) {
            this.process = process;
            this.ui = $('#UI .taskbar');
            this.textProgram = this.ui.find('#text-program');
            this.startMenu = this.ui.find('.start-menu');

            this.setup = function () {
                if (!_UI.isSmartphone())
                    this.ui.find('.program-list-task').niceScroll();
                return this;
            }

            this.getProgram = function (process) {
                return this.ui.find('div[data-process="' + this.process + '"]');
            }

            this.getPrograms = function () {
                return this.ui.find('div[data-process]');
            }

            this.setActive = function (state, process) {
                if (process == undefined) process = this.process;
                if (state) {
                    this.ui.find('div[data-process]').removeClass('open');
                    $('.winForm').removeClass('focused').css('z-index', '100');
                    $('.winForm[data-process="' + process + '"]').addClass('focused');
                    this.getProgram(process).addClass('open').css('z-index', '1000');

                } else {
                    this.getProgram(process).removeClass('open');
                    $('.winForm[data-process="' + process + '"]').removeClass('focused').css('z-index', '100');
                }
                this.refresh();
                return this;
            };

            this.refresh = function () {
                var self = this;
                this.ui.find('div[data-process]').unbind('click').click(function () {
                    var process = $(this).attr('data-process');
                    if ($(this).hasClass('open')) {
                        _UI.minimizeProgram(process);
                    } else {
                        _UI.maximizeProgram(process, true);
                    }
                }).css('cursor', 'pointer');

                this.checkSpaceBar();
                return this;
            }

            this.close = function () {
                this.getProgram(this.process).remove();
                if (this.getPrograms().length == 0)
                    this.textProgram.show();
                return this;
            }

            this.addProgram = function (app) {
                this.textProgram.hide();
                var icon = './Programs/' + app.processName + '/' + app.icon;
                this.ui.find('.program-list-task').append('<div data-process="' + app.processName + '" class="program-task open">\
                    <span class="appname">' + app.name + '</span></div>');

                var pos = this.ui.find('.program-task.open').position();
                this.ui.find('.program-list-task').scrollLeft(pos.left);


                this.refresh();
                return this;
            }

            this.checkSpaceBar = function () {
                var dispo = this.ui.width() - (this.ui.find('.start-menu').width() + this.ui.find('#time-system').width() + 40);
                this.ui.find('.program-list-task').css({
                    width: dispo + 'px'
                })


                // this.ui.find('.program-list-task').perfectScrollbar('update');
                return this;
            }

            return this;
        }

        this.initUI = function () {
            this.GridApp = $('.grid-app');
            this.setupDesktop();
            return this;
        };

        this.closeProgram = function (process) {
            this.getFormProcess(process).addClass('zoomOut animated').one(this.animationEnd, function () {
                $(this).remove();
            });
            this.Taskbar(process).close();
        }

        this.minimizeProgram = function (process) {
            var form = this.getFormProcess(process),
                app = MaterialOS.ProgramsList[process],
                self = this;

            form.removeClass('state-maximized').addClass('zoomOut animated').one(this.animationEnd, function () {
                $(this).removeClass('zoomOut animated').addClass('state-minimized');
                self.Taskbar(process).setActive(false);
            });
        }

        this.fixPositionWindows = function () {
            var self = this;
            // Taskbar
            this.Taskbar().refresh().getPrograms().each(function () {
                var process = $(this).attr('data-process');
                self.fixPositionWindow(process);
            });

            // Screen
            var dispoHeight = $(window).height() - ($('.taskbar').height());
            if ($('.screener').height() > dispoHeight) {
                $('.screener').css({
                    maxHeight: dispoHeight + 'px',
                    overflowY: 'auto'
                })
            } else {
                $('.screener').removeAttr('style');
            }
        }

        this.fixPositionWindow = function (process) {
            var form = this.getFormProcess(process),
                app = MaterialOS.ProgramsList[process];

            var leftForm = parseInt(form.css('left').split('px')[0]),
                topForm = parseInt(form.css('top').split('px')[0]),
                wForm = form.width(),
                hForm = form.height(),
                wWindow = $(window).width(),
                hWindow = $(window).height();

            if (wForm + leftForm > wWindow) {
                form.css({
                    left: '0%',
                    width: '100%'
                });
            }

            if ((hForm + topForm) - 40 > wWindow) {
                // form.css({
                //     top: '51px',
                //     height: (hWindow - 51) + 'px'
                // });
            }

        }

        this.maximizeProgram = function (process, justFocus) {
            var form = this.getFormProcess(process),
                app = MaterialOS.ProgramsList[process];

            form.removeClass('state-minimized').removeClass('zoomIn').addClass('zoomIn animated').one(this.animationEnd, function () {
                $(this).removeClass('zoomIn animated');
            });

            this.Taskbar(process).setActive(true);
            if (!justFocus) {
                if (form.hasClass('state-maximized')) {
                    form.attr('style', form.attr('data-lstyle')).removeClass('state-maximized').removeAttr('data-lstyle');
                } else {

                    form.attr('data-lstyle', form.attr('style'));
                    form.css({
                        top: '50px',
                        left: '0px',
                        height: ($(window).height() - 51) + 'px',
                        width: '100%'
                    }).addClass('state-maximized').removeClass('hide');
                }
            }
            this.fixPositionWindow(process);
        }

        this.setupDesktop = function () {
            var self = this;
            for (var ib in MaterialOS.Config.UserSettings.Desktop.Shortcut) {
                for (var i in MaterialOS.ProgramsList) {
                    if (i == MaterialOS.Config.UserSettings.Desktop.Shortcut[ib]) {
                        var app = MaterialOS.ProgramsList[i];
                        this.GridApp.append('<div style="max-width: ' + MaterialOS.Config.UserSettings.Desktop.IconMaxSize + 'px"  data-process="' + i + '" class="shortcut-app"> \
                    <img src="' + MaterialOS.Paths.Programs + i + '/' + app.icon + '"/><label>' + app.name + '</label>');
                    }
                }
            }

            this.Taskbar().setup();

            $(window).resize(function () {
                self.fixPositionWindows();
                self.refreshDynamicElementPosition();
            });

            this.initShortcutAction();
            this.initTriggerButton();

            setTimeout(function () {
                // $('.start-menu').trigger('click');
            }, 1000);
            return this;
        };

        this.refreshDynamicElementPosition = function () {
            var winWidth = $(window).width(),
                winHeight = $(window).height();

            $('#materialos').height(winHeight);
            $('#materialos #UI').height(winHeight);
            $('.start-menu-form .bottom-fix').width(winWidth);
            $('.start-menu-form .menu-element').css('max-height', (winHeight - ($('.start-menu-form .bottom-fix').height() + 130)));


        }

        this.initShortcutAction = function () {
            var self = this;
            this.GridApp.find('.shortcut-app').unbind('click').click(function () {
                if ($(this).hasClass('waitopen')) return;
                var process = $(this).data('process');
                self.GridApp.find('.shortcut-app[data-process="' + process + '"]').addClass('waitopen');
                self.openProgram(process);
            }).hover(function () {
                if ($(this).hasClass('waitopen')) return;
                $(this).addClass('pulse animated');
            }, function () {
                $(this).removeClass('pulse animated');
            });

            for (var ob in MaterialOS.Config.UserSettings.OpenOnBoot) {
                var process = MaterialOS.Config.UserSettings.OpenOnBoot[ob];
                $('div.shortcut-app[data-process="' + process + '"]').trigger('click');
            }

            return this;
        };

        this.openProgram = function (process) {
            var msg = MaterialOS.SystemCenterEvent('Ouverture de ' + process + ' en cours...'),
                app = MaterialOS.ProgramsList[process],
                self = this;
            setTimeout(function () {
                $.get(this.Paths.Programs + process + '/' + app.rootTemplate).done(function (data) {
                    MaterialOS.ProgramsList[process].RootTemplate = data;
                    $.getScript(MaterialOS.Paths.Programs + process + '/' + app.rootJs).done(function () {
                        self.createWindowProgram(process);
                        setTimeout(function () {
                            msg.close();
                        }, 700);
                        self.GridApp.find('.shortcut-app[data-process="' + process + '"]').removeClass('waitopen');
                    }).fail(function () {
                        msg.close();
                        MaterialOS.SystemAlert('Erreur lors de l\'ouverture du programme.', 'warning', 3000);
                        self.GridApp.find('.shortcut-app[data-process="' + process + '"]').removeClass('waitopen');
                    });
                }).fail(function () {
                    msg.close();
                    MaterialOS.SystemAlert('Erreur lors de l\'ouverture du programme.', 'warning', 3000);
                    self.GridApp.find('.shortcut-app[data-process="' + process + '"]').removeClass('waitopen');
                })
            }, 1000);
        };

        this.getFormProcess = function (process) {
            return MaterialOS.Container.find('div.winForm[data-process="' + process + '"]');
        }

        this.setProgramTitle = function (name) {
            $('.taskbar #text-program').html(name);
            return this;
        }

        this.addTaskProgramOpenned = function (app) {
            this.Taskbar().addProgram(app);
            return this;
        }

        this.setFocusProgram = function (app) {
            this.getFormProcess(app.processName).addClass('focused');
            return this;
        }

        this.createWindowProgram = function (process) {
            var win = MaterialOS.Container.find('div.winForm[data-process="' + process + '"]'),
                app = MaterialOS.ProgramsList[process],
                self = this;

            app.processName = process;

            if (win.length > 0) { // Une instance existe déjà.
                MaterialOS.SystemAlert('Une instance du programme est déjà ouverte.', 'warning', 3000);
                return;
            }


            MaterialOS.Container.append('<div class="winForm zoomIn animated" data-process="' + process + '" \
                style="position:absolute;min-height:' + app.window.minHeight + '; \
                min-width:' + app.window.minWidth + ';\
                width:' + app.window.width + ';\
                height:' + app.window.height + ';\
                z-index: 1000;\
                ">' + app.RootTemplate + '</div>');
            win = MaterialOS.Container.find('div.winForm[data-process="' + process + '"]');

            win.one(self.animationEnd, function () {
                $(this).removeClass('zoomIn animated');
            }).mousedown(function () {
                if ($(this).hasClass('focused')) return;
                self.Taskbar(process).setActive(true);
            })

            this.Taskbar(process).setActive(true);

            this.addTaskProgramOpenned(app).setFocusProgram(app);

            var positionLeft = ($(window).width() / 2) - (win.width() / 2),
                positionTop = ((($(window).height() - 50) / 2) - (win.height() / 2));

            if (positionTop < 50) positionTop = 50;

            win.css({
                left: positionLeft + 'px',
                top: positionTop + 'px'
            });

            if (app.window.resizable) {
                win.find('.win-body').append('<div class="e-resizable"></div>');
                win.resizable({
                    handle: 'div.winForm[data-process="' + process + '"] .e-resizable',
                    containment: 'body',
                    opacity: '0.8',
                    minWidth: 200,
                    minHeight: 200,
                    resize: function (e, ui) {
                        $(ui.element).find('.win-body').css({
                            height: $(ui.element).height() - ($(ui.element).find('.win-title').height() + 20)
                        })
                    }
                });
            }

            if (app.window.movable) {
                win.draggable({
                    cursor: 'move',
                    opacity: '0.8',
                    containment: 'body',
                    handle: '.win-title h4',
                    stop: function (e, ui) {
                        if ($(ui.helper).position().top < 51)
                            $(ui.helper).css('top', '51px')
                    }
                });
            }


            if (app.window.controlBox) {
                win.find('.win-title').prepend('<i class="fa fa-window-maximize pull-right fa-2x onClickMaximize"></i>\
                <i class="fa fa-window-minimize pull-right fa-2x onClickMinimize"></i>');

                win.find('.win-title .onClickMaximize').unbind().on('click touch', function () {
                    self.maximizeProgram(process);
                });
                win.find('.win-title .onClickMinimize').unbind().on('click touch', function () {
                    self.minimizeProgram(process);
                });
            } else {
                win.find('.win-title .fa-window-maximize, .win-title .fa-window-minimize').remove();
            }

            win.find('.win-title').prepend('<i class="fa fa-window-close pull-right fa-2x onClickCloseProgram"></i>');
            win.find('.win-title .onClickCloseProgram').unbind().on('click touch', function () {
                self.closeProgram(process);
            });

        };

        this.initTriggerButton = function () {
            var self = this;
            $('button[name="__System_Reboot"]').unbind().click(function () {
                window.location.reload();
            });

            $('.start-menu').unbind().click(function () {
                if (self.uiStartMenu.hasClass('hide')) {
                    self.addDarkBg(function () {
                        $('.start-menu').trigger('click');
                    }).refreshDynamicElementPosition();

                    self.uiStartMenu.removeClass('hide').addClass('slideInLeft animated fast-animated').one(self.animationEnd, function () {
                        self.uiStartMenu.find('.bottom-fix').hide().fadeIn(300);
                        self.uiStartMenu.attr('class', 'start-menu-form');
                    });

                } else {
                    self.uiStartMenu.removeClass('slideInLeft animated').addClass('slideOutLeft animated fast-animated').one(self.animationEnd, function () {
                        self.uiStartMenu.attr('class', 'start-menu-form').addClass('hide');
                        self.removeDarkBg();
                    })
                }
            }).css('cursor', 'pointer');
        }

        this.addDarkBg = function (onClick) {
            if ($('.darkbg').length > 0) return;
            $('#materialos #UI').append('<div class="darkbg"></div>');
            if (onClick != undefined) $('#darkbg').click(onClick).css('cursor', 'pointer');
            return this;
        }

        this.removeDarkBg = function () {
            $('.darkbg').remove();
            return this;
        }

        return this;
    });
    var UI = MaterialOS.Controllers.UI().initUI();
    MaterialOS.UI = UI;
});